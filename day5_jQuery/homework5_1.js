function validateEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

function validate() {
  var $result = $("#result");
  var email = $("#emailinput").val();
  console.log
  $result.text("");

  if (validateEmail(email)) {} else {
    $result.text("email is not valid format");
    $result.css("color", "red");
  }
  return false;
}

$("#emailsubmit").on("click", validate);

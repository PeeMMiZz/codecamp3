fetch('/day2_html/homework2_1.json').then(response => {
  return response.json();
}).then(employees => {
    let peopleLowSalary = employees.filter(lessThanHundredThousand).map(x => ({
      ...x,
      salary: x.salary*2
    }));
    let peopleHighSalary = employees.filter(moreThanHundredThousand).map(x => ({
      ...x
    }));
    let allemployees = [...peopleLowSalary,...peopleHighSalary]
    let result = allemployees.reduce((sum, number) => {return sum+parseInt(number.salary)},0);
    $('#myTable2').append($('<tr>'));
    Object.keys(allemployees[0]).forEach(x => $('#myTable2').append($('<th>' + x + '</th>')))
    Object.keys(allemployees).forEach(x => {
      $('#myTable2').append($('<tr>'));
      Object.keys(allemployees[x]).forEach(y => $('#myTable2').append($('<td>'+ allemployees[x][y] + '</td>')))
    })
    $('#myTable2').append($('<tr><td></td><td></td><td></td><td>Sum Salary</td><td>' + result + '</td></tr>'));
}).catch(err => {
  console.error('Error:', err);
});

function lessThanHundredThousand (salary) {
    return salary.salary<100000;
}

function moreThanHundredThousand (salary) {
  return salary.salary>=100000;
}